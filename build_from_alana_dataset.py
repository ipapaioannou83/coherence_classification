import itertools
import json
import random
from argparse import ArgumentParser
import logging
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import os
from alana_dataset_utils import extract_segment
import pandas as pd

logging.basicConfig(level=logging.INFO)

LABELS = {'excellent': 1,
          'passable': 1,
          'poor': 0,
          'good': 1,
          'notGood': 0}

class DatasetBuilder:
    def __init__(self):
        self.root_path = os.path.dirname(os.path.abspath(__file__))
        self.annotations = None

    def load_annotations(self, filename):
        logging.info(f"Reading data from {filename}")
        self.annotations = pd.read_csv(filename)
        return self.annotations

    @staticmethod
    def load_data(data):
        logging.info(f"Reading data from {data}")
        d = extract_segment.load_alana_dataset(data)
        logging.info(f"Read {len(d)} from {data}")
        return d

    def save_dataset(self, out, data):
        with open(os.path.join(self.root_path, out), 'w') as f:
            f.write(json.dumps(data, indent=4))
            logging.info(f"Saved dataset in {out}")

    @staticmethod
    def _draw_random_negative_sample(conversation_id, data):
        while True:
            # Pick a random conversation
            c = random.choice(list(data.keys()))

            # Pick a random turn
            t = random.choice(data[c])['u2']

            if conversation_id != c and t:
                return t

    @staticmethod
    def _extract_topic(conv):
        return ' '.join(
            max([x for x in conv['article_url'].split('/') if not x.endswith('_story.html')][3:], key=len).split('-'))

    def build_p_original_ds(self, data):
        """build positive samples"""
        logging.info("Building positive samples dataset...")
        samples = extract_segment.get_rating_filtered(data=data, min_rating=3.0, operator='gt')
        logging.info(f"{len(samples)} positive samples extracted")
        random.shuffle(samples)

        n_dataset = extract_segment.get_user_system_pairs(data=samples, preprocessed=True)
        train_l, validate_l = train_test_split(list(n_dataset.items()), test_size=0.1)
        train = {}
        for x in tqdm(train_l):
            try:
                idx = 0
                conv = []
                for c in x[1]:
                    # print(self.annotations[self.annotations.session_id == x[0]][self.annotations.turn_number == idx]['is_coherent'].bool())
                    conv.append({'u1': c['user'],
                                 'u2': c['system'],
                                 'label': LABELS[self.annotations[self.annotations.session_id == x[0]][self.annotations.turn_number == idx].alexaResponseQuality.item()]
                                 })
                    idx += 1
                train[x[0]] = conv
            except ValueError:
                continue

        validate = {}
        for x in tqdm(validate_l):
            try:
                idx = 0
                conv = []
                for c in x[1]:
                    conv.append({'u1': c['user'],
                                 'u2': c['system'],
                                 'label': 1 if
                                           self.annotations[self.annotations.session_id == x[0]][
                                               self.annotations.turn_number == idx]['is_coherent'].bool() == True
                                           else 0

                                 })
                    idx += 1
                validate[x[0]] = conv
            except ValueError:
                continue

        # train = [{x[0]: [{'u1': c['user'],
        #                   'u2': c['system'],
        #                   'label': (1 if
        #                             self.annotations[self.annotations.session_id == x[0]][
        #                                 self.annotations.turn_number == 3]['is_coherent'].bool() == True
        #                             else 0
        #                             )
        #                   } for c in x[1]] for x in train_l}][0]
        # validate = [{x[0]: [{'u1': c['user'], 'u2': c['system'], 'label': 1} for c in x[1]] for x in validate_l}][0]
        logging.info(f"{len(n_dataset)} positive examples generated")
        logging.info(f"Training set: {len(train)}")
        logging.info(f"Testing set: {len(validate)}")
        return train, validate

    @staticmethod
    def build_n_original_ds(data):
        """build negative samples"""
        logging.info("Building negative samples dataset...")
        samples = extract_segment.get_rating_filtered(data=data, min_rating=1.0, operator='eq')
        logging.info(f"{len(samples)} negative samples extracted")
        random.shuffle(samples)

        n_dataset = extract_segment.get_user_system_pairs(data=samples, preprocessed=True)
        train_l, validate_l = train_test_split(list(n_dataset.items()), test_size=0.1)
        train = [{x[0]: [{'u1': c['user'], 'u2': c['system'], 'label': 0} for c in x[1]] for x in train_l}][0]
        validate = [{x[0]: [{'u1': c['user'], 'u2': c['system'], 'label': 0} for c in x[1]] for x in validate_l}][0]
        logging.info(f"{len(n_dataset)} negative examples generated")
        logging.info(f"Training set: {len(train)}")
        logging.info(f"Testing set: {len(validate)}")
        return train, validate


def main(**kwargs):
    builder = DatasetBuilder()
    data = builder.load_data(kwargs.get('data'))
    annotations = builder.load_annotations(kwargs.get('annotations'))
    train_dataset, validate_dataset = builder.build_p_original_ds(data)
    # n_samples_train, n_samples_test = builder.build_n_original_ds(data)
    # train_dataset = {**p_samples_train, **n_samples_train}
    # validate_dataset = {**p_samples_test, **n_samples_test}
    logging.info(f"Total training dataset size: {len(train_dataset)}")
    logging.info(f"Total testing dataset size: {len(validate_dataset)}")
    builder.save_dataset(kwargs.get('train'), train_dataset)
    builder.save_dataset(kwargs.get('validate'), validate_dataset)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-d', '--data', type=str, required=True)
    ap.add_argument('--annotations', type=str, required=True)
    ap.add_argument('--validate', type=str, default='validate.json')
    ap.add_argument('--train', type=str, default='train.json')
    ap.add_argument('--test', type=str, default='test.json')
    ap.add_argument('-o', '--out', type=str, default='dataset.json')
    ap.add_argument('--neg-sample', type=float, default=0.5)
    ap.add_argument('-r', '--raw', action='store_true', help='If feeding the unaltered Topical Chat dataset')
    ap.add_argument('-nt', '--no-topic', action='store_true', help='Do not include topic')
    # ap.add_argument('-uid', '--userid', type=str, default='dummy-user')
    # ap.add_argument('-cv', '--console-verbosity', default='info', help='Console logging verbosity')
    # ap.add_argument('-fv', '--file-verbosity', default='debug', help='File logging verbosity')
    # ap.add_argument('-l', '--logfile', default='logs/alana.log', help='Path to the log file')
    # ap.add_argument('-w', '--workers', default=8, help='Number of workers')
    # ap.add_argument('-d', '--debug', action='store_true', help='1 worker & 1hr timeout')
    # ap.add_argument('--override-sessid',
    #                 help='Connect to a specific session ID and continue (console only, use with caution!)')

    args = ap.parse_args()
    main(
        annotations=args.annotations,
        data=args.data,
        validate=args.validate,
        train=args.train,
        test=args.test,
        neg_sample=args.neg_sample,
        raw=args.raw,
        output=args.out,
        no_topic=args.no_topic,
    )
