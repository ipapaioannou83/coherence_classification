{
    "dataset_reader": {
        "lazy": true,
        "type": "topical-chat-n",
        "history_length": 5,
        "skip_label_indexing": false,
        "max_sequence_length": 30,
        "token_indexers": {
              "tokens": {
                  "type": "single_id",
                  "lowercase_tokens": true
              },
              "elmo": {
                  "type": "elmo_characters"
              }
          }
    },
    "vocabulary": {
        "directory_path": "/scratch/ip9/experiments/coherence_metric/vocab/vocabulary"
    },
    "train_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.train",
    "validation_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.validate",
    "model": {
        "type": "coherence-classifier",//"engagement-classifier",
        "class_weights": [0.0003, 0.1027, 0.8830, 0.9997, 0.9918],  // https://discuss.pytorch.org/t/normalize-a-vector-to-0-1/14594/2
        "dropout": 0.1,
        "text_field_embedder": {
              "tokens": {
                "type": "embedding",
                "embedding_dim": 100,
                "pretrained_file": "https://allennlp.s3.amazonaws.com/datasets/glove/glove.6B.100d.txt.gz",
                "trainable": false
              },
              "elmo": {
                "type": "elmo_token_embedder",
                "options_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
                "weight_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
                "do_layer_norm": false,
                "dropout": 0.5
              }
            },
        "self_att_encoder": {
              "type": "stacked_self_attention",
              "input_dim": 2248,
              "hidden_dim": 2248,
              "projection_dim": 1124,
              "feedforward_hidden_dim": 1124,
              "num_layers": 1,
              "num_attention_heads": 4
        },
        "seq2seq_encoder": {
              "type": "lstm",
              "bidirectional": true,
              "input_size": 1124,
              "hidden_size": 1124,
//              "num_layers": 2,
        },
//      "seq2vec_encoder": {
//              "type": "lstm",
//              "bidirectional": true,
//              "input_size": 4496,
//              "hidden_size": 4496,
//              "num_layers": 2,
//              "dropout": 0.2
//        },
//        "similarity_function": {
//              "type": "linear",
//              "combination": "x,y,x*y",
//              "tensor_1_dim": 2248,
//              "tensor_2_dim": 2248
//        },
        "classifier": {
                "input_dim": 4496,
                "num_layers": 4,
                "hidden_dims": [2248, 512, 512, 5],
                "activations": ["relu", "relu", "relu", "linear"],
                "dropout": [0.1, 0.1, 0.1, 0.0]
        }
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["tokens1", "num_tokens"], ["tokens2", "num_tokens"]],
        "batch_size": 24,
        "biggest_batch_first": true
    },
    "trainer": {
        "optimizer": {
            "type": "adagrad",
        },
        "validation_metric": "+f1-avg",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 10,
        "grad_norm": 10.0,
        "patience": 10,
        "cuda_device": [1]
    }
}