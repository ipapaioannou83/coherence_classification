/** You could basically use this config to train your own BERT classifier,
    with the following changes:

    1. change the `bert_model` variable to "bert-base-uncased" (or whichever you prefer)
    2. swap in your own DatasetReader. It should generate instances
       that look like {"tokens": TextField(...), "label": LabelField(...)}.

       You don't need to add the "[CLS]" or "[SEP]" tokens to your instances,
       that's handled automatically by the token indexer.
    3. replace train_data_path and validation_data_path with real paths
    4. any other parameters you want to change (e.g. dropout)
 */


# For a real model you'd want to use "bert-base-uncased" or similar.
#local bert_model = "allennlp/tests/fixtures/bert/vocab.txt";

{
    "dataset_reader": {
        "lazy": false,
        "type": "bert-reader",
        "neg_sample_rate": 0.5,
        "bert_vocab": "bert-base-uncased",
    },
    "train_data_path": "/scratch/ip9/experiments/amazon_train_for_exp.json",
    "validation_data_path": "/scratch/ip9/experiments/freq_out_for_exp.json",
    "model": {
        "type": "bert_for_classification_edited",
        "bert_model": "bert-base-uncased",
        "dropout": 0.0,
        "num_labels": 2
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["tokens", "num_tokens"]],
        "batch_size": 64,
        "biggest_batch_first": true
    },
    "trainer": {
        "optimizer": {
            "type": "adam",
            "lr": 0.001
        },
        "validation_metric": "+accuracy",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 50,
        "grad_norm": 10.0,
        "patience": 5,
        "cuda_device": 0
    }
}
