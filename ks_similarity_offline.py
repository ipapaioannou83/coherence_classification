import os
from argparse import ArgumentParser
import logging
import numpy as np
import scipy as sp
from allennlp.commands.elmo import ElmoEmbedder
import json
import heapq
from nltk.tokenize import sent_tokenize

from allennlp.common.file_utils import cached_path
from tqdm import tqdm


def flatten(S):
    if S == []:
        return S
    if isinstance(S[0], list):
        return flatten(S[0]) + flatten(S[1:])
    return S[:1] + flatten(S[1:])


class Similarity:
    def __init__(self, knowledge_file, file_path):
        with open(cached_path(knowledge_file), "r") as ks_file:
            self._knowledge_source = json.load(ks_file)
        with open(cached_path(file_path), "r") as data_file:
            self.data = json.load(data_file)

        self.ee = ElmoEmbedder()

    def read_ks(self):
        for cid, conv in list(self.data.items()):
            knowledge = []
            for turn in tqdm(conv['content']):

                # Get knowledge source from reading set for this session id
                ks = self._knowledge_source[cid][turn['agent']]
                for entity in list(ks.values()):
                    knowledge.extend(entity.get('fun_facts'))
                    knowledge.extend([entity.get('summarized_wiki_lead_section', '')])
                    knowledge.extend([entity.get('shortened_wiki_lead_section', '')])

                # Get articles from knowledge source
                articles = []
                for k, v in list(self._knowledge_source[cid]['article'].items()):
                    if "AS" in k:
                        articles.append(v)
                knowledge.extend(articles)
                knowledge = flatten(knowledge)
                knowledge = [sent_tokenize(x) for x in knowledge]
                # flatten_knowledge = [item for sublist in knowledge for item in sublist]
                flatten_knowledge = flatten(knowledge)
                print(turn['knowledge_source'])

                text = (turn['message'] if turn['message'] else '')
                # print(np.squeeze(self.ee.embed_sentence(text.split())[2, 0, :]))
                # print(np.squeeze(self.ee.embed_sentence(knowledge[1].split())[2, 0, :]))
                similarities = [sp.spatial.distance.cosine(
                    np.squeeze(self.ee.embed_sentence(text.split())[2, 0, :]),
                    np.squeeze(self.ee.embed_sentence(x.split())[2, 0, :])
                ) for x in flatten_knowledge if x is not '']

                passage = [flatten_knowledge[similarities.index(n)] for n in heapq.nlargest(5, similarities)]
                print(f"\ntext: {text}\npassage: {passage}\n")


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-d', '--data', type=str, required=True)
    ap.add_argument('-ks', '--knowledge-source', type=str, required=True)

    args = ap.parse_args()

    similarity = Similarity(knowledge_file=args.knowledge_source, file_path=args.data)
    similarity.read_ks()
