{
    "dataset_reader": {
        "lazy": false,
        "type": "sentence-pair-direct",
        "skip_label_indexing": true,
        "token_indexers": {
              "tokens": {
                  "type": "single_id",
                  "lowercase_tokens": true
              },
              "elmo": {
                  "type": "elmo_characters"
              }
          }
    },
    "train_data_path": "/scratch/ip9/experiments/coherence_metric/coherence_classification/finetune_train.json",
    "validation_data_path": "/scratch/ip9/experiments/coherence_metric/coherence_classification/finetune_validate.json",
    "model": {
        "type": "coherence-classifier",
        "dropout": 0.0,
        "num_labels": 2,
        "text_field_embedder": {
              "tokens": {
                "type": "embedding",
                "embedding_dim": 100,
                "pretrained_file": "https://allennlp.s3.amazonaws.com/datasets/glove/glove.6B.100d.txt.gz",
                "trainable": false
              },
              "elmo": {
                "type": "elmo_token_embedder",
                "options_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
                "weight_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
                "do_layer_norm": false,
                "dropout": 0.5
              }
            },
//        "seq2seq_encoder": {
//              "type": "stacked_self_attention",
//              "feedforward_hidden_dim": 128,
//              "hidden_dim": 1124,
//              "input_dim": 1124,
//              "num_attention_heads": 8,
//              "num_layers": 1,
//              "projection_dim": 128
//        },
        "seq2vec_encoder": {
              "type": "lstm",
              "bidirectional": true,
              "input_size": 1124,
              "hidden_size": 1124,
              "num_layers": 1,
              "dropout": 0.2
        },
        "classifier": {
                "input_dim": 4496,
                "num_layers": 4,
                "hidden_dims": [2248, 512, 128, 2],
                "activations": ["relu", "relu", "relu", "linear"],
                "dropout": [0.2, 0.2, 0.2, 0.0]
        }
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["tokens1", "num_tokens"], ["tokens2", "num_tokens"]],
        "batch_size": 76,
        "biggest_batch_first": false
    },
    "trainer": {
        "optimizer": {
            "type": "adagrad",
        },
        "validation_metric": "+accuracy",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 50,
        "grad_norm": 10.0,
        "patience": 10,
        "cuda_device": 0
    }
}