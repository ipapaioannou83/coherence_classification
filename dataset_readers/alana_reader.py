from alana_dataset_utils import extract_segment
from typing import Dict, Union
import logging
import json

from overrides import overrides
from allennlp.common.file_utils import cached_path
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import TextField, Field, ListField, ArrayField, LabelField, MetadataField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.tokenizers import Tokenizer, WordTokenizer
from allennlp.data.tokenizers.sentence_splitter import SpacySentenceSplitter
import os.path

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@DatasetReader.register("alana-reader")
class AlanaReader(DatasetReader):
    def __init__(self,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Tokenizer = None,
                 max_sequence_length: int = None,
                 history_length: int = 3,
                 truncate_history_only: bool = False,
                 skip_label_indexing: bool = True,  # Since I am using numerical labels
                 lazy: bool = False) -> None:
        super().__init__(lazy=lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._max_sequence_length = max_sequence_length
        self._skip_label_indexing = skip_label_indexing
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self._hist_length = history_length
        self._truncate_history_only = truncate_history_only

    @overrides
    def _read(self, file_path):
        # with open(cached_path(file_path), "r") as data_file:
        cid_rating = {}
        mapping = {}
        for line in open(cached_path(file_path), "r"):
            l = json.loads(line)
            cid_rating[l['session_id']] = l.get('rating')
            D = extract_segment.get_user_system_pairs(data=[l], preprocessed=True)
            for sid, conv in list(D.items()):
                ctx = ['|']
                for turn in conv:
                    for speaker in ['user', 'system']:
                        text1 = ' | '.join(ctx[-self._hist_length:])
                        # print(text1)
                        text2 = turn[speaker]
                        if self._max_sequence_length is not None:
                            ctx.append(' '.join(self._truncate(text2.split())))
                        else:
                            ctx.append(text2)

                        instance = self.text_to_instance(text1=text1, text2=text2, sid=sid, speaker=speaker)
                        if instance is not None:
                            yield instance

    def _truncate(self, tokens):
        """
        truncate a set of tokens using the provided sequence length
        """
        if len(tokens) > self._max_sequence_length:
            tokens = tokens[:self._max_sequence_length]
        return tokens

    @overrides
    def text_to_instance(self, text1: str, text2: str,
                         label: Union[str, int, float] = None,
                         sid: str = None, speaker: str = None) -> Instance:  # type: ignore

        fields: Dict[str, Field] = {}
        tokens1 = self._tokenizer.tokenize(text1)
        tokens2 = self._tokenizer.tokenize(text2)
        if self._max_sequence_length is not None:
        #     tokens1 = self._truncate(tokens1)
            tokens2 = self._truncate(tokens2)
        fields["sid"] = MetadataField(sid)
        fields["context"] = MetadataField(text1)
        fields["utterance"] = MetadataField(text2)
        fields["speaker"] = MetadataField(speaker)
        fields['tokens1'] = TextField(tokens1, self._token_indexers)
        fields['tokens2'] = TextField(tokens2, self._token_indexers)
        if label is not None:
            fields['label'] = LabelField(label, skip_indexing=self._skip_label_indexing)
        return Instance(fields)

