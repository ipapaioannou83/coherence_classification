from typing import Dict, List, Union
import logging
import json
import random

from overrides import overrides
from allennlp.common.file_utils import cached_path
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import TextField, Field, ListField, ArrayField, LabelField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.tokenizers import Tokenizer, WordTokenizer
from allennlp.data.tokenizers.sentence_splitter import SpacySentenceSplitter

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

# For BERT use [CLS] and [SEP]
DEFAULT_SEP_TOKEN = ""
DEFAULT_INIT_TOKEN = ""


@DatasetReader.register("sentence-pair-direct")
class SentencePairReaderSplit(DatasetReader):
    """
    Reads tokens and their labels from a labeled text classification dataset.
    Expects a "text" field and a "label" field in JSON format.

    The output of ``read`` is a list of ``Instance`` s with the fields:
        tokens: ``TextField`` and
        label: ``LabelField``

    Parameters
    ----------
    token_indexers : ``Dict[str, TokenIndexer]``, optional
        optional (default=``{"tokens": SingleIdTokenIndexer()}``)
        We use this to define the input representation for the text.
        See :class:`TokenIndexer`.
    tokenizer : ``Tokenizer``, optional (default = ``{"tokens": WordTokenizer()}``)
        Tokenizer to use to split the input text into words or other kinds of tokens.
    segment_sentences: ``bool``, optional (default = ``False``)
        If True, we will first segment the text into sentences using SpaCy and then tokenize words.
        Necessary for some models that require pre-segmentation of sentences, like the Hierarchical
        Attention Network (https://www.cs.cmu.edu/~hovy/papers/16HLT-hierarchical-attention-networks.pdf).
    max_sequence_length: ``int``, optional (default = ``None``)
        If specified, will truncate tokens to specified maximum length.
    skip_label_indexing: ``bool``, optional (default = ``False``)
        Whether or not to skip label indexing. You might want to skip label indexing if your
        labels are numbers, so the dataset reader doesn't re-number them starting from 0.
    lazy : ``bool``, optional, (default = ``False``)
        Whether or not instances can be read lazily.
    """

    def __init__(self,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Tokenizer = None,
                 max_sequence_length: int = None,
                 skip_label_indexing: bool = True,  # Since I am using numerical labels
                 delimiter: str = DEFAULT_SEP_TOKEN,
                 init_token: str = DEFAULT_INIT_TOKEN,
                 lazy: bool = False) -> None:
        super().__init__(lazy=lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._max_sequence_length = max_sequence_length
        self._skip_label_indexing = skip_label_indexing
        self._delimiter = delimiter
        self._init_token = init_token
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}

    @overrides
    def _read(self, file_path):
        with open(cached_path(file_path), "r") as data_file:
            data = json.load(data_file)
            keys = list(data.keys())
            random.shuffle(keys)
            for key in keys:
                for turn in data[key]:
                    text1 = turn['u1']
                    text2 = (turn['u2'] if turn['u2'] else '[NONE]')
                    label = turn['label']

                    instance = self.text_to_instance(text1=text1, text2=text2, label=label)
                    if instance is not None:
                        yield instance

    def _truncate(self, tokens):
        """
        truncate a set of tokens using the provided sequence length
        """
        if len(tokens) > self._max_sequence_length:
            tokens = tokens[:self._max_sequence_length]
        return tokens

    @overrides
    def text_to_instance(self, text1: str, text2: str,
                         label: Union[str, int, float] = None) -> Instance:  # type: ignore
        """
        Parameters
        ----------
        text1 : ``str``, required.
            The text to classify
        text2 : ``str``, required.
            The text to classify
        label : ``str``, optional, (default = None).
            The label for this text.

        Returns
        -------
        An ``Instance`` containing the following fields:
            tokens : ``TextField``
                The tokens in the sentence or phrase.
            label : ``LabelField``
                The label label of the sentence or phrase.
        """
        # pylint: disable=arguments-differ
        fields: Dict[str, Field] = {}
        tokens1 = self._tokenizer.tokenize(text1)
        tokens2 = self._tokenizer.tokenize(text2)
        if self._max_sequence_length is not None:
            tokens1 = self._truncate(tokens1)
            tokens2 = self._truncate(tokens2)
        fields['tokens1'] = TextField(tokens1, self._token_indexers)
        fields['tokens2'] = TextField(tokens2, self._token_indexers)
        if label is not None:
            fields['label'] = LabelField(label, skip_indexing=self._skip_label_indexing)
        return Instance(fields)
