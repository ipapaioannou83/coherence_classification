from typing import Dict, Union
import logging
import json

from overrides import overrides
from allennlp.common.file_utils import cached_path
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import TextField, Field, ListField, ArrayField, LabelField, MetadataField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.tokenizers import Tokenizer, WordTokenizer
from allennlp.data.tokenizers.sentence_splitter import SpacySentenceSplitter
import os.path

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@DatasetReader.register("topical-chat-n")
class TopicalChatHistory(DatasetReader):
    """
    Reads tokens and their labels from a labeled text classification dataset.
    Expects a "text" field and a "label" field in JSON format.

    The output of ``read`` is a list of ``Instance`` s with the fields:
        tokens: ``TextField`` and
        label: ``LabelField``

    Parameters
    ----------
    token_indexers : ``Dict[str, TokenIndexer]``, optional
        optional (default=``{"tokens": SingleIdTokenIndexer()}``)
        We use this to define the input representation for the text.
        See :class:`TokenIndexer`.
    tokenizer : ``Tokenizer``, optional (default = ``{"tokens": WordTokenizer()}``)
        Tokenizer to use to split the input text into words or other kinds of tokens.
    segment_sentences: ``bool``, optional (default = ``False``)
        If True, we will first segment the text into sentences using SpaCy and then tokenize words.
        Necessary for some models that require pre-segmentation of sentences, like the Hierarchical
        Attention Network (https://www.cs.cmu.edu/~hovy/papers/16HLT-hierarchical-attention-networks.pdf).
    max_sequence_length: ``int``, optional (default = ``None``)
        If specified, will truncate tokens to specified maximum length.
    skip_label_indexing: ``bool``, optional (default = ``False``)
        Whether or not to skip label indexing. You might want to skip label indexing if your
        labels are numbers, so the dataset reader doesn't re-number them starting from 0.
    lazy : ``bool``, optional, (default = ``False``)
        Whether or not instances can be read lazily.
    """

    def __init__(self,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Tokenizer = None,
                 max_sequence_length: int = None,
                 history_length: int = 3,
                 truncate_history_only: bool = False,
                 skip_label_indexing: bool = True,  # Since I am using numerical labels
                 lazy: bool = False) -> None:
        super().__init__(lazy=lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._max_sequence_length = max_sequence_length
        self._skip_label_indexing = skip_label_indexing
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self._hist_length = history_length
        self._truncate_history_only = truncate_history_only

    @overrides
    def _read(self, file_path):
        with open(cached_path(file_path), "r") as data_file:
            data = json.load(data_file)
            for _, conv in list(data.items()):
                ctx = ['|']
                null_labels = [turn['turn_rating'] for turn in conv['content'] if turn['turn_rating'] == '']
                if len(null_labels) > 1:
                    continue  # skip dialogues with no annotations
                else:
                    for turn in conv['content']:
                        text1 = ' | '.join(ctx[-self._hist_length:])
                        # print(text1)
                        text2 = (turn['message'] if turn['message'] else '[NONE]')
                        label = turn['turn_rating'].lower() or 'passable'
                        if self._max_sequence_length is not None:
                            ctx.append(' '.join(self._truncate(text2.split())))
                        else:
                            ctx.append(text2)

                        instance = self.text_to_instance(text1=text1, text2=text2, label=label)
                        if instance is not None:
                            yield instance

    def _truncate(self, tokens):
        """
        truncate a set of tokens using the provided sequence length
        """
        if len(tokens) > self._max_sequence_length:
            tokens = tokens[:self._max_sequence_length]
        return tokens

    @overrides
    def text_to_instance(self, text1: str, text2: str,
                         label: Union[str, int, float] = None,
                         sid: Union[str, int, float] = None,
                         speaker: Union[str, int] = None) -> Instance:  # type: ignore
        """
        Parameters
        ----------
        text1 : ``str``, required.
            The text to classify
        text2 : ``str``, required.
            The text to classify
        label : ``str``, optional, (default = None).
            The label for this text.

        Returns
        -------
        An ``Instance`` containing the following fields:
            tokens : ``TextField``
                The tokens in the sentence or phrase.
            label : ``LabelField``
                The label label of the sentence or phrase.
        """
        fields: Dict[str, Field] = {}
        fields["context"] = MetadataField(text1)
        fields["utterance"] = MetadataField(text2)
        fields["sid"] = MetadataField(sid)
        fields["speaker"] = MetadataField(speaker)
        tokens1 = self._tokenizer.tokenize(text1)
        tokens2 = self._tokenizer.tokenize(text2)
        if self._max_sequence_length is not None:
        #     tokens1 = self._truncate(tokens1)
            tokens2 = self._truncate(tokens2)
        fields['tokens1'] = TextField(tokens1, self._token_indexers)
        fields['tokens2'] = TextField(tokens2, self._token_indexers)
        if label is not None:
            fields['label'] = LabelField(label, skip_indexing=self._skip_label_indexing)
        return Instance(fields)


@DatasetReader.register("topical-coherence")
class TopicalChatTopicalCoherence(DatasetReader):
    """
    Reads tokens and their labels from a labeled text classification dataset.
    Expects a "text" field and a "label" field in JSON format.

    The output of ``read`` is a list of ``Instance`` s with the fields:
        tokens: ``TextField`` and
        label: ``LabelField``

    Parameters
    ----------
    token_indexers : ``Dict[str, TokenIndexer]``, optional
        optional (default=``{"tokens": SingleIdTokenIndexer()}``)
        We use this to define the input representation for the text.
        See :class:`TokenIndexer`.
    tokenizer : ``Tokenizer``, optional (default = ``{"tokens": WordTokenizer()}``)
        Tokenizer to use to split the input text into words or other kinds of tokens.
    segment_sentences: ``bool``, optional (default = ``False``)
        If True, we will first segment the text into sentences using SpaCy and then tokenize words.
        Necessary for some models that require pre-segmentation of sentences, like the Hierarchical
        Attention Network (https://www.cs.cmu.edu/~hovy/papers/16HLT-hierarchical-attention-networks.pdf).
    max_sequence_length: ``int``, optional (default = ``None``)
        If specified, will truncate tokens to specified maximum length.
    skip_label_indexing: ``bool``, optional (default = ``False``)
        Whether or not to skip label indexing. You might want to skip label indexing if your
        labels are numbers, so the dataset reader doesn't re-number them starting from 0.
    lazy : ``bool``, optional, (default = ``False``)
        Whether or not instances can be read lazily.
    """

    def __init__(self,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Tokenizer = None,
                 max_sequence_length: int = None,
                 history_length: int = 3,
                 knowledge_source_file: str = None,
                 skip_label_indexing: bool = True,  # Since I am using numerical labels
                 lazy: bool = False) -> None:
        super().__init__(lazy=lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._max_sequence_length = max_sequence_length
        self._skip_label_indexing = skip_label_indexing
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self._hist_length = history_length
        self._knowledge_source_file = knowledge_source_file
        self._seperator = ' # '

        assert os.path.exists(self._knowledge_source_file), "Knowledge Source file missing"
        with open(self._knowledge_source_file, "r") as ks_file:
            self._knowledge_source = json.load(ks_file)

    @overrides
    def _read(self, file_path):
        with open(cached_path(file_path), "r") as data_file:
            data = json.load(data_file)
            for cid, conv in list(data.items()):
                # Skip dialogues where there is not knowledge source
                if not self._knowledge_source.get(cid):
                    continue
                for turn in conv['content']:

                    # Get knowledge source from reading set for this session id
                    ks = self._knowledge_source[cid][turn['agent']]
                    sh_wiki, ff, sum_wiki = [], [], []
                    for entity in list(ks.values()):
                        ff.extend(entity.get('fun_facts'))
                        sum_wiki.extend([entity.get('summarized_wiki_lead_section', '')])
                        sh_wiki.extend([entity.get('shortened_wiki_lead_section', '')])

                    # Get articles from knowledge source
                    articles = []
                    for k, v in list(self._knowledge_source[cid]['article'].items()):
                        if "AS" in k:
                            articles.append(v)

                    ff = self._seperator.join(ff)
                    sum_wiki = self._seperator.join(sum_wiki)
                    sh_wiki = self._seperator.join(sh_wiki)
                    articles = self._seperator.join(articles) or self._seperator

                    text2 = (turn['message'] if turn['message'] else '[NONE]')

                    # labels-> 0 (negative): personal knowledge, 1: personal knowledge + FSx, 2 (gold): FSx
                    if len(turn['knowledge_source']) == 0:
                        label = 0
                    elif 'Personal Knowledge' in turn['knowledge_source']:
                        if len(turn['knowledge_source']) == 1:
                            label = 0
                        else:
                            label = 1
                    else:
                        label = 2

                    # logging.info(f"{ff}\n{sum_wiki}\n{sh_wiki}\n{articles}\n{text2}\n{label}")
                    instance = self.text_to_instance(ks1=ff,
                                                     ks2=sum_wiki,
                                                     ks3=sh_wiki,
                                                     ks4=articles,
                                                     text2=text2,
                                                     label=label)
                    if instance is not None:
                        yield instance

    def _truncate(self, tokens):
        """
        truncate a set of tokens using the provided sequence length
        """
        if len(tokens) > self._max_sequence_length:
            tokens = tokens[:self._max_sequence_length]
        return tokens

    @overrides
    def text_to_instance(self, ks1: str,
                         ks2: str,
                         ks3: str,
                         ks4: str,
                         text2: str,
                         label: Union[str, int, float] = None) -> Instance:  # type: ignore
        """
        Parameters
        ----------
        text1 : ``str``, required.
            The text to classify
        text2 : ``str``, required.
            The text to classify
        label : ``str``, optional, (default = None).
            The label for this text.

        Returns
        -------
        An ``Instance`` containing the following fields:
            tokens : ``TextField``
                The tokens in the sentence or phrase.
            label : ``LabelField``
                The label label of the sentence or phrase.
        """
        fields: Dict[str, Field] = {}
        ks1 = self._tokenizer.tokenize(ks1)
        ks2 = self._tokenizer.tokenize(ks2)
        ks3 = self._tokenizer.tokenize(ks3)
        ks4 = self._tokenizer.tokenize(ks4)
        tokens2 = self._tokenizer.tokenize(text2)
        if self._max_sequence_length is not None:
            #     tokens1 = self._truncate(tokens1)
            tokens2 = self._truncate(tokens2)
        fields['ks1'] = TextField(ks1, self._token_indexers)
        fields['ks2'] = TextField(ks2, self._token_indexers)
        fields['ks3'] = TextField(ks3, self._token_indexers)
        fields['ks4'] = TextField(ks4, self._token_indexers)
        fields['tokens2'] = TextField(tokens2, self._token_indexers)
        if label is not None:
            fields['label'] = LabelField(label, skip_indexing=self._skip_label_indexing)
        return Instance(fields)
