import json
from tqdm import tqdm
import sys

cid_rating = {}
for line in open(sys.argv[1]):
    l = json.loads(line)
    cid_rating[l['session_id']] = l.get('rating')

with open("actual_ratings.json", "w") as f:
    f.write(json.dumps(cid_rating, indent=4))

