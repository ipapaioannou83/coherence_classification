import logging
from allennlp.models import load_archive
from allennlp.predictors import Predictor
import json
from argparse import ArgumentParser
import coherence_classification
from tqdm import tqdm
from alana_dataset_utils import extract_segment
import random


class Simulator:
    def __init__(self, model=None):
        logging.basicConfig()
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        if not model:
            self.logger.critical("No model provided. Exiting")
            exit(1)

        self.logger.info(f"Loaded model from {model}")
        self.model = load_archive(model)
        self.predictor = Predictor.from_archive(self.model, 'coherence-predictor')
        self.logger.info(f"Loaded model from {model}")

    def simulate_cli(self):
        i = 0
        while True:
            self.logger.info(f"Turn {i}\n")
            u1 = input('u1: ')
            u2 = input('u2: ')

            result = self.predictor.predict_json({"u1": u1, "u2": u2})
            self.logger.info(f"Result: {json.dumps(result, indent=4)}")

    def test_alana_dataset(self, dataset, sample_size=10, output=None):
        res = ""
        head = extract_segment.load_alana_dataset(dataset)[20:100]
        testingset = []
        while len(testingset) < sample_size:
            for i in head:
                keep = True if random.random() > 0.5 else False
                if keep:
                    testingset.append(i)

        D = extract_segment.get_user_system_pairs(data=testingset, preprocessed=True)
        for _, conv in tqdm(list(D.items()), desc="Predicting labels..."):
            for turn in conv:
                result = self.predictor.predict_json({"u1": turn['user'], "u2": turn['system']})
                res += f"U1: {turn['user']}, U2: {turn['system']} || Result: {result['label']}\n"

        with open(output, 'w') as f:
            f.write(res)


if __name__ == '__main__':
    argp = ArgumentParser()
    argp.add_argument('--model', type=str)
    argp.add_argument('--cli', action='store_true')
    argp.add_argument('-d', '--data', type=str)
    argp.add_argument('-o', '--output', type=str, default='sim_output.txt')
    args = argp.parse_args()

    sim = Simulator(model=args.model)
    if args.cli:
        sim.simulate_cli()
    else:
        sim.test_alana_dataset(dataset=args.data, output=args.output)
