from typing import Dict, Optional, Union, List

from allennlp.modules.matrix_attention import LegacyMatrixAttention
from overrides import overrides
import torch

from allennlp.data import Vocabulary
from allennlp.models.model import Model
from allennlp.modules import Seq2SeqEncoder, Seq2VecEncoder, TextFieldEmbedder, FeedForward, SimilarityFunction, \
    TimeDistributed, Highway
from allennlp.nn import InitializerApplicator, RegularizerApplicator
from allennlp.nn.util import get_text_field_mask, masked_softmax, weighted_sum, replace_masked_values
from allennlp.training.metrics import CategoricalAccuracy, F1Measure, FBetaMeasure
from coherence_classification.f_measure import F1


@Model.register("coherence-classifier")
class CoherenceClassifier(Model):
    """
    This ``Model`` implements a basic text classifier. After embedding the text into
    a text field, we will optionally encode the embeddings with a ``Seq2SeqEncoder``. The
    resulting sequence is pooled using a ``Seq2VecEncoder`` and then passed to
    a linear classification layer, which projects into the label space. If a
    ``Seq2SeqEncoder`` is not provided, we will pass the embedded text directly to the
    ``Seq2VecEncoder``.

    Parameters
    ----------
    vocab : ``Vocabulary``
    text_field_embedder : ``TextFieldEmbedder``
        Used to embed the input text into a ``TextField``
    seq2seq_encoder : ``Seq2SeqEncoder``, optional (default=``None``)
        Optional Seq2Seq encoder layer for the input text.
    seq2vec_encoder : ``Seq2VecEncoder``
        Required Seq2Vec encoder layer. If `seq2seq_encoder` is provided, this encoder
        will pool its output. Otherwise, this encoder will operate directly on the output
        of the `text_field_embedder`.
    dropout : ``float``, optional (default = ``None``)
        Dropout percentage to use.
    num_labels: ``int``, optional (default = ``None``)
        Number of labels to project to in classification layer. By default, the classification layer will
        project to the size of the vocabulary namespace corresponding to labels.
    label_namespace: ``str``, optional (default = "labels")
        Vocabulary namespace corresponding to labels. By default, we use the "labels" namespace.
    initializer : ``InitializerApplicator``, optional (default=``InitializerApplicator()``)
        If provided, will be used to initialize the model parameters.
    regularizer : ``RegularizerApplicator``, optional (default=``None``)
        If provided, will be used to calculate the regularization penalty during training.
    """

    def __init__(
            self,
            vocab: Vocabulary,
            text_field_embedder: TextFieldEmbedder,
            seq2vec_encoder: Seq2VecEncoder = None,
            seq2seq_encoder: Seq2SeqEncoder = None,
            self_att_encoder: Seq2SeqEncoder = None,
            dropout: float = None,
            num_labels: int = None,
            label_namespace: str = "labels",
            initializer: InitializerApplicator = InitializerApplicator(),
            regularizer: Optional[RegularizerApplicator] = None,
            classifier: FeedForward = None,
            class_weights: List = None
    ) -> None:

        super().__init__(vocab, regularizer)
        self._text_field_embedder = text_field_embedder

        if seq2seq_encoder:
            self._seq2seq_encoder = seq2seq_encoder
        else:
            self._seq2seq_encoder = None

        if self_att_encoder:
            self._self_att_encoder = self_att_encoder
        else:
            self._self_att_encoder = None

        if seq2vec_encoder:
            self._seq2vec_encoder = seq2vec_encoder
        else:
            self._seq2vec_encoder = None

        # Classifier layer's output dim is the concat or each seq2vec of text1, text2
        # self._classifier_input_dim = 2*self._seq2vec_encoder.get_output_dim()

        if dropout:
            self._dropout = torch.nn.Dropout(dropout)
        else:
            self._dropout = None

        self._label_namespace = label_namespace

        if num_labels:
            self._num_labels = num_labels
        else:
            self._num_labels = vocab.get_vocab_size(namespace=self._label_namespace)

        self._class_weights = torch.Tensor(class_weights)
        self._classification_layer = classifier
        self._f1 = F1(vocabulary=vocab)
        self._accuracy = CategoricalAccuracy()
        self._loss = torch.nn.CrossEntropyLoss(weight=self._class_weights)  # torch.nn.MSELoss()
        initializer(self)

    def forward(  # type: ignore
            self, tokens1: Dict[str, torch.LongTensor],
            sid: List,
            speaker: List,
            tokens2: Dict[str, torch.LongTensor],
            label: torch.IntTensor = None
    ) -> Dict[str, torch.Tensor]:

        """
        Parameters
        ----------
        tokens1 : Dict[str, torch.LongTensor]
            From a ``TextField``
        tokens2 : Dict[str, torch.LongTensor]
            From a ``TextField``
        label : torch.IntTensor, optional (default = None)
            From a ``LabelField``

        Returns
        -------
        An output dictionary consisting of:

        logits : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            unnormalized log probabilities of the label.
        probs : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            probabilities of the label.
        loss : torch.FloatTensor, optional
            A scalar loss to be optimised.
        """
        embedded_text1 = self._text_field_embedder(tokens1)
        embedded_text2 = self._text_field_embedder(tokens2)
        mask1 = get_text_field_mask(tokens1).float()
        mask2 = get_text_field_mask(tokens2).float()

        if self._seq2seq_encoder:
            embedded_text1 = self._seq2seq_encoder(embedded_text1, mask=mask1)
            embedded_text2 = self._seq2seq_encoder(embedded_text2, mask=mask2)

        if self._self_att_encoder:
            embedded_text1 = self._self_att_encoder(embedded_text1, mask=mask1)  # history
            embedded_text2 = self._self_att_encoder(embedded_text2, mask=mask2)

        if self._seq2vec_encoder:
            embedded_text1 = self._seq2vec_encoder(embedded_text1, mask=mask1)
            embedded_text2 = self._seq2vec_encoder(embedded_text2, mask=mask2)
        else:
            embedded_text1 = torch.sum(embedded_text1 * mask1.unsqueeze(dim=-1).float(), dim=1)
            embedded_text2 = torch.sum(embedded_text2 * mask2.unsqueeze(dim=-1).float(), dim=1)

        if self._dropout:
            embedded_text1 = self._dropout(embedded_text1)
            embedded_text2 = self._dropout(embedded_text2)

        embedded_text = torch.cat([embedded_text1, embedded_text2], dim=-1)

        logits = self._classification_layer(embedded_text)
        probs = torch.nn.functional.softmax(logits, dim=-1)

        output_dict = {"logits": logits, "probs": probs, "sid": sid, "speaker": speaker}

        if label is not None:
            loss = self._loss(logits, label.long().view(-1))
            output_dict["loss"] = loss
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict

    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        """
        Does a simple argmax over the probabilities, converts index to string label, and
        add ``"label"`` key to the dictionary with the result.
        """
        predictions = output_dict["probs"]
        if predictions.dim() == 2:
            predictions_list = [predictions[i] for i in range(predictions.shape[0])]
        else:
            predictions_list = [predictions]
        classes = []
        for prediction in predictions_list:
            label_idx = prediction.argmax(dim=-1).item()
            label_str = self.vocab.get_index_to_token_vocabulary(self._label_namespace).get(
                label_idx, str(label_idx)
            )
            classes.append(label_str)
        output_dict["label"] = classes
        return output_dict

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        accuracy = {"accuracy": self._accuracy.get_metric(reset)}
        f1 = self._f1.get_metric(reset)
        metrics = {**accuracy, **f1}

        return metrics


@Model.register("engagement-classifier")
class CoherenceClassifierEngagement(Model):
    """
    This ``Model`` implements a basic text classifier. After embedding the text into
    a text field, we will optionally encode the embeddings with a ``Seq2SeqEncoder``. The
    resulting sequence is pooled using a ``Seq2VecEncoder`` and then passed to
    a linear classification layer, which projects into the label space. If a
    ``Seq2SeqEncoder`` is not provided, we will pass the embedded text directly to the
    ``Seq2VecEncoder``.

    Parameters
    ----------
    vocab : ``Vocabulary``
    text_field_embedder : ``TextFieldEmbedder``
        Used to embed the input text into a ``TextField``
    seq2seq_encoder : ``Seq2SeqEncoder``, optional (default=``None``)
        Optional Seq2Seq encoder layer for the input text.
    seq2vec_encoder : ``Seq2VecEncoder``
        Required Seq2Vec encoder layer. If `seq2seq_encoder` is provided, this encoder
        will pool its output. Otherwise, this encoder will operate directly on the output
        of the `text_field_embedder`.
    dropout : ``float``, optional (default = ``None``)
        Dropout percentage to use.
    num_labels: ``int``, optional (default = ``None``)
        Number of labels to project to in classification layer. By default, the classification layer will
        project to the size of the vocabulary namespace corresponding to labels.
    label_namespace: ``str``, optional (default = "labels")
        Vocabulary namespace corresponding to labels. By default, we use the "labels" namespace.
    initializer : ``InitializerApplicator``, optional (default=``InitializerApplicator()``)
        If provided, will be used to initialize the model parameters.
    regularizer : ``RegularizerApplicator``, optional (default=``None``)
        If provided, will be used to calculate the regularization penalty during training.
    """

    def __init__(
            self,
            vocab: Vocabulary,
            text_field_embedder: TextFieldEmbedder,
            similarity_function: SimilarityFunction,
            seq2vec_encoder: Seq2VecEncoder = None,
            seq2seq_encoder: Seq2SeqEncoder = None,
            self_att_encoder: Seq2SeqEncoder = None,
            dropout: float = None,
            num_labels: int = None,
            label_namespace: str = "labels",
            initializer: InitializerApplicator = InitializerApplicator(),
            regularizer: Optional[RegularizerApplicator] = None,
            classifier: FeedForward = None,
            class_weights: List = None
    ) -> None:

        super().__init__(vocab, regularizer)
        self._text_field_embedder = text_field_embedder
        self._matrix_attention = LegacyMatrixAttention(similarity_function)
        self._class_weights = torch.Tensor(class_weights)
        # print(">>>>", self._class_weights, type(self._class_weights))

        if seq2seq_encoder:
            self._seq2seq_encoder = seq2seq_encoder
        else:
            self._seq2seq_encoder = None

        if self_att_encoder:
            self._self_att_encoder = self_att_encoder
        else:
            self._self_att_encoder = None

        if seq2vec_encoder:
            self._seq2vec_encoder = seq2vec_encoder
        else:
            self._seq2vec_encoder = None

        if dropout:
            self._dropout = torch.nn.Dropout(dropout)
        else:
            self._dropout = None

        self._label_namespace = label_namespace

        if num_labels:
            self._num_labels = num_labels
        else:
            self._num_labels = vocab.get_vocab_size(namespace=self._label_namespace)

        self._classification_layer = classifier
        self._f1 = F1(vocabulary=vocab)
        self._accuracy = CategoricalAccuracy()
        self._loss = torch.nn.CrossEntropyLoss(weight=self._class_weights)  # torch.nn.MSELoss()
        initializer(self)

    def forward(  # type: ignore
            self,
            utterance: List,
            context: List,
            sid: List,
            speaker: List,
            tokens1: Dict[str, torch.LongTensor],
            tokens2: Dict[str, torch.LongTensor],
            label: torch.IntTensor = None
    ) -> Dict[str, torch.Tensor]:

        """
        Parameters
        ----------
        tokens1 : Dict[str, torch.LongTensor]
            From a ``TextField``
        tokens2 : Dict[str, torch.LongTensor]
            From a ``TextField``
        label : torch.IntTensor, optional (default = None)
            From a ``LabelField``

        Returns
        -------
        An output dictionary consisting of:

        logits : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            unnormalized log probabilities of the label.
        probs : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            probabilities of the label.
        loss : torch.FloatTensor, optional
            A scalar loss to be optimised.
        """
        embedded_text1 = self._text_field_embedder(tokens1)
        embedded_text2 = self._text_field_embedder(tokens2)
        mask1 = get_text_field_mask(tokens1).float()
        mask2 = get_text_field_mask(tokens2).float()

        if self._seq2seq_encoder:
            embedded_text1 = self._seq2seq_encoder(embedded_text1, mask=mask1)  # history
            embedded_text2 = self._seq2seq_encoder(embedded_text2, mask=mask2)

        if self._self_att_encoder:
            embedded_text1 = self._self_att_encoder(embedded_text1, mask=mask1)  # history
            embedded_text2 = self._self_att_encoder(embedded_text2, mask=mask2)

        if self._dropout:
            embedded_text1 = self._dropout(embedded_text1)
            embedded_text2 = self._dropout(embedded_text2)

        # Attention between current turn and encoded history
        sentences_attention = self._calculate_attention(encoded_passage=embedded_text1,
                                                        encoded_question=embedded_text2,
                                                        passage_mask=mask1,
                                                        question_mask=mask2,
                                                        batch_size=embedded_text1.size(0),
                                                        passage_length=embedded_text1.size(1))

        if self._seq2vec_encoder:
            attended_matrix = self._seq2vec_encoder(sentences_attention)
        else:
            attended_matrix = torch.sum(sentences_attention, dim=1)

        logits = self._classification_layer(attended_matrix)
        probs = torch.nn.functional.softmax(logits, dim=-1)

        output_dict = {"logits": logits, "probs": probs, "sid": sid, "speaker": speaker, "utterance": utterance, "context": context}

        if label is not None:
            loss = self._loss(logits, label.long().view(-1))
            output_dict["loss"] = loss
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict

    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        """
        Does a simple argmax over the probabilities, converts index to string label, and
        add ``"label"`` key to the dictionary with the result.
        """
        predictions = output_dict["probs"]
        if predictions.dim() == 2:
            predictions_list = [predictions[i] for i in range(predictions.shape[0])]
        else:
            predictions_list = [predictions]
        classes = []
        for prediction in predictions_list:
            label_idx = prediction.argmax(dim=-1).item()
            label_str = self.vocab.get_index_to_token_vocabulary(self._label_namespace).get(
                label_idx, str(label_idx)
            )
            classes.append(label_str)
        output_dict["label"] = classes
        return output_dict

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        accuracy = {"accuracy": self._accuracy.get_metric(reset)}
        f1 = self._f1.get_metric(reset)
        metrics = {**accuracy, **f1}

        return metrics

    def _calculate_attention(self,
                             encoded_passage,
                             encoded_question,
                             passage_mask,
                             question_mask,
                             batch_size,
                             passage_length):
        encoding_dim = encoded_question.size(-1)
        # Shape: (batch_size, passage_length, question_length)
        passage_question_similarity = self._matrix_attention(encoded_passage, encoded_question)
        # Shape: (batch_size, passage_length, question_length)
        passage_question_attention = masked_softmax(passage_question_similarity, question_mask)
        # Shape: (batch_size, passage_length, encoding_dim)
        passage_question_vectors = weighted_sum(encoded_question, passage_question_attention)

        # We replace masked values with something really negative here, so they don't affect the
        # max below.
        masked_similarity = replace_masked_values(passage_question_similarity,
                                                  question_mask.unsqueeze(1),
                                                  -1e7)
        # Shape: (batch_size, passage_length)
        question_passage_similarity = masked_similarity.max(dim=-1)[0].squeeze(-1)
        # Shape: (batch_size, passage_length)
        question_passage_attention = masked_softmax(question_passage_similarity, passage_mask)
        # Shape: (batch_size, encoding_dim)
        question_passage_vector = weighted_sum(encoded_passage, question_passage_attention)
        # Shape: (batch_size, passage_length, encoding_dim)
        tiled_question_passage_vector = question_passage_vector.unsqueeze(1).expand(batch_size,
                                                                                    passage_length,
                                                                                    encoding_dim)

        # Shape: (batch_size, passage_length, encoding_dim * 4)
        final_merged_passage = torch.cat([encoded_passage,
                                          passage_question_vectors,
                                          encoded_passage * passage_question_vectors,
                                          encoded_passage * tiled_question_passage_vector],
                                         dim=-1)
        return final_merged_passage


@Model.register("topical-coherence")
class CoherenceClassifierTopical(Model):
    """
    This ``Model`` implements a basic text classifier. After embedding the text into
    a text field, we will optionally encode the embeddings with a ``Seq2SeqEncoder``. The
    resulting sequence is pooled using a ``Seq2VecEncoder`` and then passed to
    a linear classification layer, which projects into the label space. If a
    ``Seq2SeqEncoder`` is not provided, we will pass the embedded text directly to the
    ``Seq2VecEncoder``.

    Parameters
    ----------
    vocab : ``Vocabulary``
    text_field_embedder : ``TextFieldEmbedder``
        Used to embed the input text into a ``TextField``
    seq2seq_encoder : ``Seq2SeqEncoder``, optional (default=``None``)
        Optional Seq2Seq encoder layer for the input text.
    seq2vec_encoder : ``Seq2VecEncoder``
        Required Seq2Vec encoder layer. If `seq2seq_encoder` is provided, this encoder
        will pool its output. Otherwise, this encoder will operate directly on the output
        of the `text_field_embedder`.
    dropout : ``float``, optional (default = ``None``)
        Dropout percentage to use.
    num_labels: ``int``, optional (default = ``None``)
        Number of labels to project to in classification layer. By default, the classification layer will
        project to the size of the vocabulary namespace corresponding to labels.
    label_namespace: ``str``, optional (default = "labels")
        Vocabulary namespace corresponding to labels. By default, we use the "labels" namespace.
    initializer : ``InitializerApplicator``, optional (default=``InitializerApplicator()``)
        If provided, will be used to initialize the model parameters.
    regularizer : ``RegularizerApplicator``, optional (default=``None``)
        If provided, will be used to calculate the regularization penalty during training.
    """

    def __init__(
            self,
            vocab: Vocabulary,
            text_field_embedder: TextFieldEmbedder,
            num_highway_layers: int,
            similarity_function: SimilarityFunction,
            seq2vec_encoder: Seq2VecEncoder = None,
            seq2seq_encoder: Seq2SeqEncoder = None,
            dropout: float = None,
            num_labels: int = None,
            label_namespace: str = "labels",
            initializer: InitializerApplicator = InitializerApplicator(),
            regularizer: Optional[RegularizerApplicator] = None,
            classifier: FeedForward = None,
            class_weights: List = None
    ) -> None:

        super().__init__(vocab, regularizer)
        self._text_field_embedder = text_field_embedder
        self._class_weights = torch.Tensor(class_weights)
        self._highway_layer = TimeDistributed(Highway(text_field_embedder.get_output_dim(),
                                                      num_highway_layers))

        if seq2seq_encoder:
            self._seq2seq_encoder = seq2seq_encoder
        else:
            self._seq2seq_encoder = None

        if seq2vec_encoder:
            self._seq2vec_encoder = seq2vec_encoder
        else:
            self._seq2vec_encoder = None

        # Classifier layer's output dim is the concat or each seq2vec of text1, text2
        # self._classifier_input_dim = 2*self._seq2vec_encoder.get_output_dim()

        self._matrix_attention = LegacyMatrixAttention(similarity_function)

        if dropout:
            self._dropout = torch.nn.Dropout(dropout)
        else:
            self._dropout = None

        self._label_namespace = label_namespace

        if num_labels:
            self._num_labels = num_labels
        else:
            self._num_labels = vocab.get_vocab_size(namespace=self._label_namespace)

        if dropout > 0:
            self._dropout = torch.nn.Dropout(p=dropout)
        else:
            self._dropout = lambda x: x
        self._classification_layer = classifier
        self._f1 = F1(vocabulary=vocab)
        self._accuracy = CategoricalAccuracy()
        self._loss = torch.nn.CrossEntropyLoss(weight=self._class_weights)  # torch.nn.MSELoss()
        initializer(self)

    def forward(  # type: ignore
            self,
            ks1: Dict[str, torch.LongTensor],
            ks2: Dict[str, torch.LongTensor],
            ks3: Dict[str, torch.LongTensor],
            ks4: Dict[str, torch.LongTensor],
            tokens2: Dict[str, torch.LongTensor],
            label: torch.IntTensor = None
    ) -> Dict[str, torch.Tensor]:

        """
        Parameters
        ----------
        tokens1 : Dict[str, torch.LongTensor]
            From a ``TextField``
        tokens2 : Dict[str, torch.LongTensor]
            From a ``TextField``
        label : torch.IntTensor, optional (default = None)
            From a ``LabelField``

        Returns
        -------
        An output dictionary consisting of:

        logits : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            unnormalized log probabilities of the label.
        probs : torch.FloatTensor
            A tensor of shape ``(batch_size, num_labels)`` representing
            probabilities of the label.
        loss : torch.FloatTensor, optional
            A scalar loss to be optimised.
        """

        embedded_ks1 = self._highway_layer(self._text_field_embedder(ks1))
        embedded_ks2 = self._highway_layer(self._text_field_embedder(ks2))
        embedded_ks3 = self._highway_layer(self._text_field_embedder(ks3))
        embedded_ks4 = self._highway_layer(self._text_field_embedder(ks4))
        embedded_text2 = self._text_field_embedder(tokens2)

        batch_size = embedded_text2.size(0)
        ks1_length = embedded_ks1.size(1)
        ks2_length = embedded_ks2.size(1)
        ks3_length = embedded_ks3.size(1)
        ks4_length = embedded_ks4.size(1)

        mask_ks1 = get_text_field_mask(ks1).float()
        mask_ks2 = get_text_field_mask(ks2).float()
        mask_ks3 = get_text_field_mask(ks3).float()
        mask_ks4 = get_text_field_mask(ks4).float()
        mask2 = get_text_field_mask(tokens2).float()

        if self._seq2seq_encoder:
            embedded_ks1 = self._dropout(self._seq2seq_encoder(embedded_ks1, mask=mask_ks1))
            embedded_ks2 = self._dropout(self._seq2seq_encoder(embedded_ks2, mask=mask_ks2))
            embedded_ks3 = self._dropout(self._seq2seq_encoder(embedded_ks3, mask=mask_ks3))
            embedded_ks4 = self._dropout(self._seq2seq_encoder(embedded_ks4, mask=mask_ks4))
            embedded_text2 = self._dropout(self._seq2seq_encoder(embedded_text2, mask=mask2))

        # Calculate attention between the utterance and each knowledge source

        # encoding_dim = embedded_text2.size(-1)

        query_aware_ks1 = self._calculate_attention(encoded_passage=embedded_ks1,
                                                    encoded_question=embedded_text2,
                                                    passage_mask=mask_ks1,
                                                    question_mask=mask2,
                                                    batch_size=batch_size,
                                                    passage_length=ks1_length)

        query_aware_ks2 = self._calculate_attention(encoded_passage=embedded_ks2,
                                                    encoded_question=embedded_text2,
                                                    passage_mask=mask_ks2,
                                                    question_mask=mask2,
                                                    batch_size=batch_size,
                                                    passage_length=ks2_length)

        query_aware_ks3 = self._calculate_attention(encoded_passage=embedded_ks3,
                                                    encoded_question=embedded_text2,
                                                    passage_mask=mask_ks3,
                                                    question_mask=mask2,
                                                    batch_size=batch_size,
                                                    passage_length=ks3_length)

        query_aware_ks4 = self._calculate_attention(encoded_passage=embedded_ks4,
                                                    encoded_question=embedded_text2,
                                                    passage_mask=mask_ks4,
                                                    question_mask=mask2,
                                                    batch_size=batch_size,
                                                    passage_length=ks4_length)

        summed_ks1 = torch.sum(query_aware_ks1 * mask_ks1.unsqueeze(dim=-1).float(), dim=1)
        summed_ks2 = torch.sum(query_aware_ks2 * mask_ks2.unsqueeze(dim=-1).float(), dim=1)
        summed_ks3 = torch.sum(query_aware_ks3 * mask_ks3.unsqueeze(dim=-1).float(), dim=1)
        summed_ks4 = torch.sum(query_aware_ks4 * mask_ks4.unsqueeze(dim=-1).float(), dim=1)
        embedded_text2 = torch.sum(embedded_text2 * mask2.unsqueeze(dim=-1).float(), dim=1)

        # if self._seq2vec_encoder:
        #     embedded_text1 = self._seq2vec_encoder(embedded_text1, mask=mask1)
        #     embedded_text2 = self._seq2vec_encoder(embedded_text2, mask=mask2)
        # else:
        #     embedded_text1 = torch.sum(embedded_text1 * mask1.unsqueeze(dim=-1).float(), dim=1)
        #     embedded_text2 = torch.sum(embedded_text2 * mask2.unsqueeze(dim=-1).float(), dim=1)

        embedded_text = torch.cat([summed_ks1,
                                   summed_ks2,
                                   summed_ks3,
                                   summed_ks4,
                                   embedded_text2], dim=-1)

        logits = self._classification_layer(embedded_text)
        probs = torch.nn.functional.softmax(logits, dim=-1)

        output_dict = {"logits": logits, "probs": probs}

        if label is not None:
            # print(f"logits: {logits}\nks4: {summed_ks4}\nlabel: {label}")
            loss = self._loss(logits, label.long().view(-1))
            output_dict["loss"] = loss
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict

    def _calculate_attention(self,
                             encoded_passage,
                             encoded_question,
                             passage_mask,
                             question_mask,
                             batch_size,
                             passage_length):
        encoding_dim = encoded_question.size(-1)
        # Shape: (batch_size, passage_length, question_length)
        passage_question_similarity = self._matrix_attention(encoded_passage, encoded_question)
        # Shape: (batch_size, passage_length, question_length)
        passage_question_attention = masked_softmax(passage_question_similarity, question_mask)
        # Shape: (batch_size, passage_length, encoding_dim)
        passage_question_vectors = weighted_sum(encoded_question, passage_question_attention)

        # We replace masked values with something really negative here, so they don't affect the
        # max below.
        masked_similarity = replace_masked_values(passage_question_similarity,
                                                  question_mask.unsqueeze(1),
                                                  -1e7)
        # Shape: (batch_size, passage_length)
        question_passage_similarity = masked_similarity.max(dim=-1)[0].squeeze(-1)
        # Shape: (batch_size, passage_length)
        question_passage_attention = masked_softmax(question_passage_similarity, passage_mask)
        # Shape: (batch_size, encoding_dim)
        question_passage_vector = weighted_sum(encoded_passage, question_passage_attention)
        # Shape: (batch_size, passage_length, encoding_dim)
        tiled_question_passage_vector = question_passage_vector.unsqueeze(1).expand(batch_size,
                                                                                    passage_length,
                                                                                    encoding_dim)

        # Shape: (batch_size, passage_length, encoding_dim * 4)
        final_merged_passage = torch.cat([encoded_passage,
                                          passage_question_vectors,
                                          encoded_passage * passage_question_vectors,
                                          encoded_passage * tiled_question_passage_vector],
                                         dim=-1)
        return final_merged_passage

    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        """
        Does a simple argmax over the probabilities, converts index to string label, and
        add ``"label"`` key to the dictionary with the result.
        """
        predictions = output_dict["probs"]
        if predictions.dim() == 2:
            predictions_list = [predictions[i] for i in range(predictions.shape[0])]
        else:
            predictions_list = [predictions]
        classes = []
        for prediction in predictions_list:
            label_idx = prediction.argmax(dim=-1).item()
            label_str = self.vocab.get_index_to_token_vocabulary(self._label_namespace).get(
                label_idx, str(label_idx)
            )
            classes.append(label_str)
        output_dict["label"] = classes
        return output_dict

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        accuracy = {"accuracy": self._accuracy.get_metric(reset)}
        f1 = self._f1.get_metric(reset)
        metrics = {**accuracy, **f1}
        return metrics
