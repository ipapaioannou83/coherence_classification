import json
from argparse import ArgumentParser
import logging
from functools import reduce
from typing import Iterator

from allennlp.common.util import prepare_environment, lazy_groups_of
from allennlp.data import Instance
from tqdm import tqdm

import coherence_classification
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.iterators import DataIterator
from allennlp.models.archival import load_archive
from allennlp.training.util import evaluate
from allennlp.predictors.predictor import Predictor
from coherence_classification.dataset_readers.alana_reader import AlanaReader

MAPPING = {
    "Poor": 1,
    "Not Good": 2,
    "Passable": 3,
    "Good": 4,
    "Excellent": 5
}


class Simulator:
    def __init__(self, **kwargs):
        logging.basicConfig()
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        if kwargs.get('model') is None:
            self.logger.critical("No model provided. Exiting")
            exit(1)
        self.model_path = kwargs.get('model')
        self.cuda = kwargs.get('cuda_device', -1)
        self.weights = kwargs.get('weights_file', None)
        self.overrides = kwargs.get('overrides', "")
        self._predictor = kwargs.get("predictor", "")
        self.output_file = kwargs.get("output", None)
        if self.overrides:
            with open(self.overrides) as f:
                self.overrides = f.read()
                # self.overrides = json.load(f)
        self.input_file = kwargs.get('input_file')

        # Load from archive
        archive = load_archive(self.model_path,
                               self.cuda,
                               self.overrides,
                               self.weights)
        self.config = archive.config
        prepare_environment(self.config)
        self.model = archive.model
        self.model.eval()

        self.logger.info(f"Loaded model from {self.model_path}")


        # Try to use the validation dataset reader if there is one - otherwise fall back
        # to the default dataset_reader used for both training and validation.
        validation_dataset_reader_params = self.config.pop('validation_dataset_reader', None)

        if validation_dataset_reader_params is not None:
            self.dataset_reader = DatasetReader.from_params(validation_dataset_reader_params)
        else:
            self.dataset_reader = DatasetReader.from_params(self.config.pop('dataset_reader'))
        self.logger.info("Reading evaluation data from %s", self.model_path)
        self.instances = self.dataset_reader.read(self.input_file)

        iterator_params = self.config.pop("validation_iterator", None)
        if iterator_params is None:
            iterator_params = self.config.pop("iterator")
        self.iterator = DataIterator.from_params(iterator_params)
        self.iterator.index_with(self.model.vocab)

    def predict(self):
        pred_interim, pred_map = {}, {}
        batch_size = self.iterator._batch_size
        predictor = Predictor.by_name(self._predictor)(self.model, self.dataset_reader)

        for batch in tqdm(lazy_groups_of(self._get_instance_data(), batch_size)):
            predictions = predictor.predict_batch_instance(batch)
            for p in predictions:
                if p.get('speaker') == 'system':
                    try:
                        pred_interim[p.get('sid')].append(MAPPING[p.get('label')])
                    except KeyError:
                        pred_interim[p.get('sid')] = []
                        pred_interim[p.get('sid')].append(MAPPING[p.get('label')])

        # Now average over the predictions to get a single score per conversation
        for k, v in list(pred_interim.items()):
            pred_map[k] = sum(v)/len(v)

        if self.output_file:
            with open(self.output_file, 'w') as f:
                f.write(json.dumps(pred_map, indent=4))
        else:
            print(f"Final predictions: \n{json.dumps(pred_map, indent=4)}")

    def evaluate(self):
        metrics = evaluate(self.model, self.instances, self.iterator, self.cuda, self.weights)

        self.logger.info("Finished evaluating.")
        self.logger.info("Metrics:")
        for key, metric in metrics.items():
            self.logger.info("%s: %s", key, metric)

    def correlation(self, predicted, actual):
        with open(predicted) as f:
            pred = json.load(f)

        act = {}
        for line in open(actual):
            l = json.loads(line)
            act[l['session_id']] = float(l['rating'])

        act = [(k, v) for k, v in list(act.items())]
        pred = [(k, v) for k, v in list(pred.items())]

    def _get_instance_data(self) -> Iterator[Instance]:
        yield from self.dataset_reader.read(self.input_file)



if __name__ == '__main__':
    argp = ArgumentParser()
    argp.add_argument('--model', type=str)
    argp.add_argument('-i', '--input-file', type=str)
    argp.add_argument('--weights-file', type=str)
    argp.add_argument('--dataset-reader', type=str)
    argp.add_argument('--overrides', type=str)
    argp.add_argument('--predictor', type=str)
    argp.add_argument('--cuda-device', type=int)
    argp.add_argument('-o', '--output', type=str, default='sim_output.txt')
    args = argp.parse_args()

    sim = Simulator(model=args.model,
                    input_file=args.input_file,
                    weights_file=args.weights_file,
                    overrides=args.overrides,
                    output=args.output,
                    predictor=args.predictor,
                    cuda_device=args.cuda_device,
                    )
    sim.predict()
    # sim.evaluate()
