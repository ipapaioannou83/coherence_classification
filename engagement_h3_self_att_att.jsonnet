{
    "dataset_reader": {
        "lazy": true,
        "type": "topical-chat-n",
        "history_length": 3,
        "skip_label_indexing": false,
        "max_sequence_length": 30,
        "token_indexers": {
              "tokens": {
                  "type": "single_id",
                  "lowercase_tokens": true
              },
              "elmo": {
                  "type": "elmo_characters"
              }
          }
    },
    "vocabulary": {
//        "max_vocab_size": 30000,
        "directory_path": "/scratch/ip9/experiments/coherence_metric/vocab/vocabulary"
    },
    "train_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.train",
    "validation_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.validate",
    "model": {
        "type": "engagement-classifier",
        "dropout": 0,
        "text_field_embedder": {
              "tokens": {
                "type": "embedding",
                "embedding_dim": 100,
                "pretrained_file": "https://allennlp.s3.amazonaws.com/datasets/glove/glove.6B.100d.txt.gz",
                "trainable": false
              },
              "elmo": {
                "type": "elmo_token_embedder",
                "options_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
                "weight_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
                "do_layer_norm": false,
                "dropout": 0.5
              }
            },
//        "seq2seq_encoder": {
//              "type": "stacked_self_attention",
//              "input_dim": 1124,
//              "hidden_dim": 1124,
//              "projection_dim": 564,
//              "feedforward_hidden_dim": 564,
//              "num_layers": 1,
//              "num_attention_heads": 4
//        },
        "seq2seq_encoder": {
              "type": "lstm",
              "bidirectional": true,
              "input_size": 1124,
              "hidden_size": 1124
        },
        "similarity_function": {
              "type": "linear",
              "combination": "x,y,x*y",
              "tensor_1_dim": 2248,
              "tensor_2_dim": 2248
        },
        "classifier": {
                "input_dim": 8992,
                "num_layers": 6,
                "hidden_dims": [4496, 2248, 1124, 512, 512, 5],
                "activations": ["relu", "relu", "relu", "relu", "relu", "linear"],
                "dropout": [0.2, 0.2, 0.2, 0.2, 0.2, 0.0]
        }
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["tokens1", "num_tokens"], ["tokens2", "num_tokens"]],
        "batch_size": 20,
        "biggest_batch_first": true
    },
    "trainer": {
        "optimizer": {
            "type": "adagrad"
        },
        "validation_metric": "+accuracy",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 50,
        "grad_norm": 10.0,
        "patience": 10,
        "cuda_device": [0]
    }
}
