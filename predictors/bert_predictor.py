from typing import Tuple

from overrides import overrides

from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.service.predictors.predictor import Predictor


@Predictor.register('bert-predictor-vanilla')
class BertClassifierPredictor(Predictor):

    @overrides
    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        text = '[CLS]' + json_dict["u1"] + '[SEP]' + json_dict["u2"] + '[SEP]'
        return self._dataset_reader.text_to_instance(text)
