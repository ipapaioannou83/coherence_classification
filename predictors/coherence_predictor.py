from typing import Tuple

from overrides import overrides

from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.service.predictors.predictor import Predictor


@Predictor.register('coherence-predictor')
class CoherencePredictor(Predictor):

    @overrides
    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        text1 = json_dict["text1"]
        text2 = json_dict["text2"]
        return self._dataset_reader.text_to_instance(text1=text1, text2=text2)
