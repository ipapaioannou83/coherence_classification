from scipy.stats import spearmanr
import json
from argparse import ArgumentParser
import logging

def correlation(data1, data2):
    corr, _ = spearmanr(data1, data2)
    print('Spearmans correlation: %.3f' % corr)


def load_data(filename):
    with open(filename) as fin:
        try:
            data = json.load(fin)
            logging.info(f"Loaded data from {filename}")
        except Exception as ex:
            logging.error(ex)
    return data


def main(**kwargs):
    dataset1 = load_data(kwargs.get('data1'))
    dataset2 = load_data(kwargs.get('data2'))
    data={}
    data1, data2 = [], []

    for k, v in list(dataset1.items()):
        try:
            data2.append(dataset2[k])
            data1.append(v)
            data[k] = {"d1": v,
                       "d2": dataset2[k]}
        except KeyError:
            pass

    correlation(data1, data2)

if __name__ == '__main__':
       argp = ArgumentParser()
       argp.add_argument('--data1', type=str)
       argp.add_argument('--data2', type=str)
       argp.add_argument('-o', '--output', type=str)
       args = argp.parse_args()

       main(data1=args.data1,
            data2=args.data2,
            output=args.output)
