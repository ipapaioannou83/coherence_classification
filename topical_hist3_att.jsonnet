{
    "dataset_reader": {
        "lazy": true,
        "type": "topical-coherence",
        "history_length": 3,
        "skip_label_indexing": true,
        "max_sequence_length": 20,
        "knowledge_source_file": "/scratch/ip9/DATA/alexa-prize-topical-chat-dataset/reading_sets/post-build/train.json",
        "token_indexers": {
              "tokens": {
                  "type": "single_id",
                  "lowercase_tokens": true
              },
              "elmo": {
                  "type": "elmo_characters"
              }
          }
    },
    "vocabulary": {
        "max_vocab_size": 30000,
        //"directory_path": "/scratch/ip9/experiments/coherence_metric/vocab_topical/vocabulary"
    },
    "train_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.train",
    "validation_data_path": "/scratch/ip9/experiments/coherence_metric/topical-chat.validate",
    "model": {
        "type": "topical-coherence",
        "class_weights": [
            1,
            1,
            1
        ],
        "dropout": 0.0,
        "num_highway_layers": 1,
        "text_field_embedder": {
              "tokens": {
                "type": "embedding",
                "embedding_dim": 100,
                "pretrained_file": "https://allennlp.s3.amazonaws.com/datasets/glove/glove.6B.100d.txt.gz",
                "trainable": false
              },
              "elmo": {
                "type": "elmo_token_embedder",
                "options_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
                "weight_file": "https://allennlp.s3.amazonaws.com/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
                "do_layer_norm": false,
                "dropout": 0.5
              }
            },
        "seq2seq_encoder": {
            "type": "lstm",
            "bidirectional": true,
            "input_size": 1124,
            "hidden_size": 1124,
            "num_layers": 1
        },
        "similarity_function": {
            "type": "linear",
            "combination": "x,y,x*y",
            "tensor_1_dim": 2248,
            "tensor_2_dim": 2248
        },
        //"seq2vec_encoder": {
        //      "type": "lstm",
        //      "bidirectional": true,
        //      "input_size": 1124,
        //      "hidden_size": 1124,
        //      "num_layers": 1,
        //      "dropout": 0.2
        //},
        "classifier": {
                "input_dim": 38216,
                "num_layers": 5,
                "hidden_dims": [4496, 2248, 2248, 1124, 3],
                "activations": ["relu", "relu", "relu", "relu", "linear"],
                "dropout": [0.2, 0.2, 0.2, 0.2, 0.0]
        }
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["ks1", "num_tokens"], ["ks2", "num_tokens"], ["ks3", "num_tokens"], ["ks4", "num_tokens"], ["tokens2", "num_tokens"]],
        "batch_size": 5,
        "biggest_batch_first": true
    },
    "trainer": {
        "optimizer": {
            "type": "adagrad",
        },
        "validation_metric": "+f1-avg",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 50,
        "grad_norm": 10.0,
        "patience": 10,
        "cuda_device": 0
    }
}
