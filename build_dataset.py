import itertools
import json
import random
from argparse import ArgumentParser
import logging
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import os

logging.basicConfig(level=logging.INFO)

TURN_RATING = {
    "not good": 1,
    "poor": 2,
    "passable": 3,
    "good": 4,
    "excellent": 5
}


class DatasetBuilder:
    def __init__(self):
        self.root_path = None

    def load_data(self, data):
        with open(data) as f:
            self.root_path, _ = os.path.split(os.path.realpath(f.name))
            logging.info(f"Reading data from {data}")
            d = json.load(f)
            logging.info(f"Read {len(d)} from {data}")
            return d

    def save_dataset(self, out, data):
        with open(os.path.join(self.root_path, out), 'w') as f:
            f.write(json.dumps(data, indent=4))
            logging.info(f"Saved dataset in {out}")

    @staticmethod
    def _draw_random_negative_sample(conversation_id, data):
        while True:
            # Pick a random conversation
            c = random.choice(list(data.keys()))

            # Pick a random turn
            t = random.choice(data[c])['u2']

            if conversation_id != c and t:
                return t

    @staticmethod
    def _extract_topic(conv):
        return ' '.join(
            max([x for x in conv['article_url'].split('/') if not x.endswith('_story.html')][3:], key=len).split('-'))

    def build_p_original_ds(self, data):
        """build positive samples"""
        logging.info("Building positive samples dataset...")
        p_dataset = {}
        for cid, conv in list(data.items()):
            values = []
            for t1, t2 in itertools.zip_longest(
                    [x['message'] for x in conv['content'] if conv['content'].index(x) % 2 == 0],
                    [x['message'] for x in conv['content'] if conv['content'].index(x) % 2 == 1]):
                label = 1
                topic = self._extract_topic(conv)
                values.append({'u1': t1, 'u2': t2, 'label': label, 'topic': topic})
            p_dataset[cid] = values
        train_l, validate_l = train_test_split(list(p_dataset.items()), test_size=0.1)
        train = [{x[0]: x[1] for x in train_l}][0]
        validate = [{x[0]: x[1] for x in validate_l}][0]
        logging.info(f"{len(p_dataset)} positive examples generated")
        logging.info(f"Training set: {len(train)}")
        logging.info(f"Testing set: {len(validate)}")
        return train, validate

    def build_n_original_ds(self, data):
        """build negative samples"""
        # First get all u2 utterances as negative samples
        logging.info("Building negative samples dataset...")
        samples = []
        for c in list(data.values()):
            for t in c['content']:
                if t['agent'] == 'agent_2':
                    samples.append(t['message'])
        logging.info(f"{len(samples)} negative samples extracted")
        random.shuffle(samples)

        n_dataset = {}
        for cid, conv in list(data.items()):
            values = []
            for t1 in [x['message'] for x in conv['content'] if conv['content'].index(x) % 2 == 0]:
                t2 = random.choice(samples)  # For each u1 utterance get one random negative sample
                label = 0
                topic = self._extract_topic(conv)
                values.append({'u1': t1, 'u2': t2, 'label': label, 'topic': topic})
            n_dataset['neg-' + cid] = values
        train_l, validate_l = train_test_split(list(n_dataset.items()), test_size=0.1)
        train = [{x[0]: x[1] for x in train_l}][0]
        validate = [{x[0]: x[1] for x in validate_l}][0]
        logging.info(f"{len(n_dataset)} negative examples generated")
        logging.info(f"Training set: {len(train)}")
        logging.info(f"Testing set: {len(validate)}")
        return train, validate

    def build_engagement_ds(self, data):
        """build positive samples"""
        logging.info("Building positive samples dataset...")
        p_dataset = {}
        for cid, conv in list(data.items()):
            skip = False
            values = []
            for t1, t2 in itertools.zip_longest(
                    [(x['message'], x['turn_rating']) for x in conv['content'] if conv['content'].index(x) % 2 == 0],
                    [(x['message'], x['turn_rating']) for x in conv['content'] if conv['content'].index(x) % 2 == 1]):
                try:
                    # label = (TURN_RATING[t1[1].lower()] + (TURN_RATING[t2[1].lower()] if t2 else 0)) / 2
                    label = min(TURN_RATING[t1[1].lower()], (TURN_RATING[t2[1].lower()] if t2 else 10)) # get the minumum between the two ratings for more neg examples
                except KeyError:
                    skip = True
                    continue
                topic = self._extract_topic(conv)
                values.append({'u1': t1[0],
                               'u2': t2[0] if t2 else '',
                               'label': label,
                               'topic': topic})
            if not skip:
                p_dataset[cid] = values
        train_l, validate_l = train_test_split(list(p_dataset.items()), test_size=0.1)
        train = [{x[0]: x[1] for x in train_l}][0]
        validate = [{x[0]: x[1] for x in validate_l}][0]
        logging.info(f"{len(p_dataset)} positive examples generated")
        logging.info(f"Training set: {len(train)}")
        logging.info(f"Testing set: {len(validate)}")
        return train, validate


def main(**kwargs):
    builder = DatasetBuilder()
    data = builder.load_data(kwargs.get('data'))
    # p_samples_train, p_samples_test = builder.build_p_original_ds(data)
    # n_samples_train, n_samples_test = builder.build_n_original_ds(data)
    train_dataset, validate_dataset = builder.build_engagement_ds(data)
    # train_dataset = {**p_samples_train, **n_samples_train}
    # validate_dataset = {**p_samples_test, **n_samples_test}
    logging.info(f"Total training dataset size: {len(train_dataset)}")
    logging.info(f"Total testing dataset size: {len(validate_dataset)}")
    builder.save_dataset(kwargs.get('train'), train_dataset)
    builder.save_dataset(kwargs.get('validate'), validate_dataset)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-d', '--data', type=str, required=True)
    ap.add_argument('--validate', type=str, default='validate.json')
    ap.add_argument('--train', type=str, default='train.json')
    ap.add_argument('--test', type=str, default='test.json')
    ap.add_argument('-o', '--out', type=str, default='dataset.json')
    ap.add_argument('--neg-sample', type=float, default=0.5)
    ap.add_argument('-r', '--raw', action='store_true', help='If feeding the unaltered Topical Chat dataset')
    ap.add_argument('-nt', '--no-topic', action='store_true', help='Do not include topic')
    # ap.add_argument('-uid', '--userid', type=str, default='dummy-user')
    # ap.add_argument('-cv', '--console-verbosity', default='info', help='Console logging verbosity')
    # ap.add_argument('-fv', '--file-verbosity', default='debug', help='File logging verbosity')
    # ap.add_argument('-l', '--logfile', default='logs/alana.log', help='Path to the log file')
    # ap.add_argument('-w', '--workers', default=8, help='Number of workers')
    # ap.add_argument('-d', '--debug', action='store_true', help='1 worker & 1hr timeout')
    # ap.add_argument('--override-sessid',
    #                 help='Connect to a specific session ID and continue (console only, use with caution!)')

    args = ap.parse_args()
    main(
        data=args.data,
        validate=args.validate,
        train=args.train,
        test=args.test,
        neg_sample=args.neg_sample,
        raw=args.raw,
        output=args.out,
        no_topic=args.no_topic,
    )
